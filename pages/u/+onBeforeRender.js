export { onBeforeRender }

async function onBeforeRender (pageContext) {
	let pageProps, erreur
	if (pageContext.role === 'utilisateur') {
		const params = pageContext.params
		const hote = pageContext.hote
		const identifiant = pageContext.identifiant
		const nom = pageContext.nom
		const email = pageContext.email
		const langue = pageContext.langue
		const role = pageContext.role
		const interactions = pageContext.interactions
		const filtre = pageContext.filtre
		const titrePage = identifiant + ' - Digistorm by La Digitale'
		pageProps = { params, hote, identifiant, nom, email, langue, role, interactions, filtre, titrePage }
	} else {
		erreur = true
		pageProps = { erreur }
	}
	return {
		pageContext: {
			pageProps
		}
	}
}
