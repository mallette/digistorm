export default {
	methods: {
		definirTabIndex () {
			return this.$parent.modale === '' && this.$parent.message === '' && this.$parent.modaleConfirmation === '' && this.modale === '' && !this.modaleQuestion ? 0 : -1
		},
		definirListe (liste) {
			liste.forEach(function (identifiant, indexIdentifiant) {
				this.utilisateurs.forEach(function (utilisateur) {
					if (identifiant === utilisateur.identifiant && utilisateur.nom !== '') {
						liste[indexIdentifiant] = utilisateur.nom
					}
				})
			}.bind(this))
			return liste.join(', ')
		},
		afficherModaleListe (liste) {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'liste'
			this.liste = this.definirListe(liste)
			this.$emit('modale', true)
			this.$nextTick(function () {
				document.querySelector('.modale .fermer').focus()
			})
		},
		fermerModaleListe () {
			this.modale = ''
			this.liste = ''
			this.$emit('modale', false)
			this.gererFocus()
		},
		afficherMedia () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'media'
			this.$emit('modale', true)
			this.$nextTick(function () {
				document.querySelector('.modale .bouton').focus()
			})
		},
		fermerModaleMedia () {
			this.modale = ''
			this.$emit('modale', false)
			this.gererFocus()
		},
		afficherImage (event, image) {
			event.preventDefault()
			event.stopPropagation()
			this.image = image
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'image'
			this.$emit('modale', true)
			this.$nextTick(function () {
				document.querySelector('.modale .bouton').focus()
			})
		},
		fermerModaleImage () {
			this.modale = ''
			this.image = ''
			this.$emit('modale', false)
			this.gererFocus()
		},
		gererFocus () {
			if (this.elementPrecedent) {
				this.elementPrecedent.focus()
				this.elementPrecedent = null
			}
		},
		gererClavier (event) {
			if (event.key === 'Escape' && this.modaleQuestion) {
				this.fermerModaleQuestion()
			} else if (event.key === 'Escape' && this.modale === 'liste') {
				this.fermerModaleListe()
			} else if (event.key === 'Escape' && this.modale === 'media') {
				this.fermerModaleMedia()
			} else if (event.key === 'Escape' && this.modale === 'image') {
				this.fermerModaleImage()
			} else if (event.key === 'Escape' && this.modale !== '') {
				this.modale = ''
				this.$emit('modale', false)
				this.gererFocus()
			}
		}
	}
}
